# Obarun        : 66 init/supervisor
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Maintainer    : YianIris <yianiris At disroot Dot org>
# Obarun PkgSrc : https://framagit.org/pkg/obextra/libinput/
#----------------
# Maintainer    : Andreas Radke <andyrtr@archlinux.org>
# Maintainer    : Jan de Groot
# Arch PkrSrc   : https://www.archlinux.org/packages/extra/x86_64/libinput/
#--------------------------------------------------------------------------
# Website       : https://www.freedesktop.org/software/libinput/
#--------------------------------------------------------------------------
#--DESCRIPTION-------------------------------------------------------------

pkgname=libinput

pkgdesc="Input device management and event handling library"

pkgver=1.17.1
pkgrel=2

url="https://freedesktop.org/software/libinput"

track=
target=$pkgname-$pkgver.tar.xz
source=($url/$target{,.sig})

#--BUILD CONFIGURATION-----------------------------------------------------

makedepends=(
    'gtk3'
    'meson')

#--BUILD CONTROL-----------------------------------------------------------

path=(
    -Dudev-dir=/usr/lib/udev
)

flags=(
    -Dtests=false
    -Ddocumentation=false
)

#--BUILD-------------------------------------------------------------------

build() {
    arch-meson $pkgname-$pkgver build "${path[@]}" "${flags[@]}"
    ninja -C build
}

#--PACKAGE-----------------------------------------------------------------

package() {
    DESTDIR="$pkgdir" ninja -C build install

    install -Dvm644 $pkgname-$pkgver/COPYING \
      "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

#--INSTALL CONFIGURATION---------------------------------------------------

arch=(x86_64)

depends=(
    'mtdev'
    'libevdev'
    'libwacom')

optdepends=('gtk3: libinput debug-gui'
            'python-pyudev: libinput measure'
            'python-libevdev: libinput measure')


#--SECURITY AND LICENCE----------------------------------------------------

license=(custom:X11)

validpgpkeys=('3C2C43D9447D5938EF4551EBE23B7E70B467F0BF') # Peter Hutterer (Who-T) <office@who-t.net>

sha512sums=('2e7baa198fed9c673d28453efb066f9371f2b575a844e41cde455c636bbe1cc68faae7129026944a502cd5d7bfcc72272066b9e3cda5c959f7b464483f9dd860'
	    'fc28ff92bcde8d9934dced8c739aaf37dd6e58ab6fe22e2fcb1c7821fc5e41b06d219a47235d38adaad6511af6d1bcab06aa022afdb1ee42dc2a9b46fe99e863'
	   )
