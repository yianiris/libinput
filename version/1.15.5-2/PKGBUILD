# Obarun        : 66 init/supervisor
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Obarun PkgSrc : https://framagit.org/pkg/obextra/libinput/
#----------------
# Maintainer    : Andreas Radke <andyrtr@archlinux.org>
# Maintainer    : Jan de Groot
# Arch PkrSrc   : https://www.archlinux.org/packages/extra/x86_64/libinput/
#--------------------------------------------------------------------------
# Website       : https://www.freedesktop.org/software/libinput/
#--------------------------------------------------------------------------
#--DESCRIPTION-------------------------------------------------------------

pkgname=libinput

pkgdesc="Input device management and event handling library"

pkgver=1.15.5
pkgrel=2

url="https://freedesktop.org/software/libinput"

track=
target=$pkgname-$pkgver.tar.xz
source=($url/$target{,.sig})

#--BUILD CONFIGURATION-----------------------------------------------------

makedepends=(
    'gtk3'
    'meson')

#--BUILD CONTROL-----------------------------------------------------------

path=(
    -Dudev-dir=/usr/lib/udev
)

flags=(
    -Dtests=false
    -Ddocumentation=false
)

#--BUILD-------------------------------------------------------------------

build() {
    arch-meson $pkgname-$pkgver build "${path[@]}" "${flags[@]}"
    ninja -C build
}

#--PACKAGE-----------------------------------------------------------------

package() {
    DESTDIR="$pkgdir" ninja -C build install

    install -Dvm644 $pkgname-$pkgver/COPYING \
      "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

#--INSTALL CONFIGURATION---------------------------------------------------

arch=(x86_64)

depends=(
    'mtdev'
    'libevdev'
    'libwacom')

optdepends=('gtk3: libinput debug-gui'
            'python-pyudev: libinput measure'
            'python-libevdev: libinput measure')

            
#--SECURITY AND LICENCE----------------------------------------------------

license=(custom:X11)

validpgpkeys=('3C2C43D9447D5938EF4551EBE23B7E70B467F0BF') # Peter Hutterer (Who-T) <office@who-t.net>

sha512sums=('634810359bc4d86f2c4c75f472bee5eaed04ed93af3c91c4bc46a596402323941f9cc90173e278bfa7e5cbda6b7a20adc59bd09520dd4152fc961302b7e57904'
            'SKIP')
